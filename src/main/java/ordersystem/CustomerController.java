package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */

    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    } 

      /**
     * Change a customer's name from the database
     * @param number the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server  
    @PutMapping("/customers/{number}")
    public ResponseEntity<Object> setCustomerNameByNumber(@PathVariable long number, @RequestBody Customer customer) {
    	if (customerDb.containsKey(number)) {
                customerDb.get(number).setFirstName(customer.getFirstName());
                customerDb.get(number).setLastName(customer.getLastName());
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
        }
    }
        
    
    /**
     * Initializes and returns an array of Customers from customerDb
     * @param customer customer with first and last names
     * @return array of all Customer objects
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @GetMapping("/customers/customerArray")
    public ResponseEntity<Object> getCustomerArray() {
        Customer[] customerArray = new Customer[(int)counter.get()];
        for(long i = 0; i < counter.get(); i++)
        {
            customerArray[(int)i] = customerDb.get(i+1);
        }
    	return new ResponseEntity<>(customerArray, HttpStatus.OK);
    }

     /**
     * Returns customer number when given a name
     * @return the customer number
     */
    @GetMapping("/customers/{firstName}/{lastName}")
    public ResponseEntity<Object> getCustomerNumberByName(@PathVariable String firstName, @PathVariable String lastName) {
        
        for(long i = 0; i < counter.get(); i++)
        {   
            if (customerDb.get(i+1).getFirstName().equals(firstName) && customerDb.get(i+1).getLastName().equals(lastName)) 
            { 
                return new ResponseEntity<>(customerDb.get(i+1).getNumber(), HttpStatus.OK);  
            }
        } 
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    }

    /**
     * Given a customer number, return the order numbers that belong to the given customer
     * @param number customer number
     * @return the order number(s)
     */
    @GetMapping("/customers/{number}/getOrders")
    public ResponseEntity<Object> getOrderLines(@PathVariable long number) {
        for(long i = 0; i < Database.getOrderCounter().get(); i++)
        {
            if (Database.getOrderDb().get(i+1).getCustomerNumber() == number) 
            {
              return new ResponseEntity<>(Database.getOrderDb().get(number).getNumber(), HttpStatus.OK);
    	    } 
        }
        return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    }
}